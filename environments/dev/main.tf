module "cluster" {
  source = "../../module"

  environment         = "dev"
  route53_domain_name = "au.awscontoso.com"

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::487523894433:user/cnnn.izzo"
      username = "cnnn.izzo"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::487523894433:user/gitlab"
      username = "gitlab"
      groups   = ["system:masters"]
    },
  ]

  network = {
    vpc_id             = "vpc-86785de1"
    public_subnet_ids  = []
    private_subnet_ids = ["subnet-05486afcf2ce84b2b", "subnet-0f13bcd7cc945c3fe"]
  }
}
