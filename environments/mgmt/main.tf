module "cluster" {
  source = "../../module"

  environment         = "mgmt"
  route53_domain_name = "au.awscontoso.com"

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::487523894433:user/cnnn.izzo"
      username = "cnnn.izzo"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::487523894433:user/gitlab"
      username = "gitlab"
      groups   = ["system:masters"]
    },
  ]

  network = {
    vpc_id             = "vpc-86785de1"
    public_subnet_ids  = ["subnet-9a5e53fd", "subnet-9ea39cd7"]
    private_subnet_ids = ["subnet-025b2ec9751a87e6c", "subnet-00a67157988833795"]
  }
}
