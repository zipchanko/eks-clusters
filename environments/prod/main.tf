module "cluster" {
  source = "../../module"

  environment         = "prod"
  route53_domain_name = "au.awscontoso.com"

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::487523894433:user/cnnn.izzo"
      username = "cnnn.izzo"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::487523894433:user/gitlab"
      username = "gitlab"
      groups   = ["system:masters"]
    },
  ]

  network = {
    vpc_id             = "vpc-86785de1"
    public_subnet_ids  = []
    private_subnet_ids = ["subnet-0a386605ca0fbed66", "subnet-0db498ead904698de"]
  }
}
