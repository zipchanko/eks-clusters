data "aws_acm_certificate" "acm_cert" {
  count = var.route53_domain_name == null ? 0 : 1

  domain      = "*.${var.route53_domain_name}"
  statuses    = ["ISSUED"]
  most_recent = true
}
