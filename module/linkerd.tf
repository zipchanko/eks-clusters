resource "helm_release" "cert_issuer" {
  name            = "cert-issuer"
  namespace       = "vault"
  chart           = "${path.module}/helm/cert-issuer"
  atomic          = true
  cleanup_on_fail = true

  depends_on = [time_sleep.vault]
}

resource "time_sleep" "cert_issuer" {
  create_duration = "30s"
  depends_on      = [helm_release.cert_issuer]
}

resource "helm_release" "linkerd" {
  name              = "linkerd"
  namespace         = "linkerd"
  chart             = "${path.module}/helm/linkerd"
  atomic            = true
  cleanup_on_fail   = true
  dependency_update = true

  depends_on = [time_sleep.cert_issuer]
}

resource "time_sleep" "linkerd" {
  create_duration = "30s"
  depends_on      = [helm_release.linkerd]
}

resource "helm_release" "linkerd_viz" {
  name              = "linkerd-viz"
  namespace         = "linkerd-viz"
  chart             = "${path.module}/helm/linkerd-viz"
  create_namespace  = true
  atomic            = true
  cleanup_on_fail   = true
  dependency_update = true

  depends_on = [time_sleep.linkerd]
}
