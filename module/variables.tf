variable "route53_domain_name" {
  description = "Route53 zone name for ACM and TLS."
  type        = string
  default     = null
}

variable "aws_auth_users" {
  description = "List of map users to cluster access."
  type        = list(any)
}

variable "environment" {
  description = "Short form of environment name."
  type        = string
}

variable "network" {
  description = "VPC and subnets."
  type = object({
    vpc_id             = string
    public_subnet_ids  = list(string)
    private_subnet_ids = list(string)
  })
}

locals {
  cluster_name   = "${var.environment}-eks"
  route53_domain = var.route53_domain_name != null ? var.route53_domain_name : ""
}
