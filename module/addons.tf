resource "time_sleep" "eks_cluster" {
  destroy_duration = "30s"
  depends_on       = [module.eks]
}

module "eks_blueprints_addons" {
  source = "aws-ia/eks-blueprints-addons/aws"

  cluster_name      = module.eks.cluster_name
  cluster_endpoint  = module.eks.cluster_endpoint
  cluster_version   = module.eks.cluster_version
  oidc_provider_arn = module.eks.oidc_provider_arn

  enable_aws_load_balancer_controller = true
  enable_cluster_autoscaler           = true
  enable_metrics_server               = true
  enable_external_dns                 = var.route53_domain_name == null ? false : true
  enable_cert_manager                 = true
  external_dns_route53_zone_arns      = var.route53_domain_name == null ? [] : [data.aws_route53_zone.zone[0].arn]

  external_dns = {
    repository    = "https://charts.bitnami.com/bitnami"
    chart_version = "6.20.4"
    values = [
      <<-EOT
        aws:
          region: ap-southeast-2
          zoneType: public
        labelFilter: "ingress in (externaldns)"
        policy: sync
        forceTxtOwnerId: true
        sources:
          - ingress
        txtOwnerId: ${local.cluster_name}
      EOT
    ]
  }

  cluster_autoscaler = {
    values = [
      <<-EOT
        extraArgs:
          scale-down-enabled: true
          scale-down-utilization-threshold: 0.8
          scale-down-delay-after-add: 1m
          skip-nodes-with-system-pods: false
      EOT
    ]
  }

  depends_on = [time_sleep.eks_cluster]
}

resource "helm_release" "ingress_nginx" {
  count = var.route53_domain_name == null ? 0 : 1

  name              = "ingress-nginx"
  namespace         = "ingress-nginx"
  chart             = "${path.module}/helm/ingress-nginx"
  create_namespace  = true
  atomic            = true
  cleanup_on_fail   = true
  dependency_update = true

  values = [
    <<-EOT
      nginx:
        ingress:
          certARN: "${data.aws_acm_certificate.acm_cert[0].arn}"
          host: "*.${tostring(var.route53_domain_name)}"
    EOT
  ]

  depends_on = [time_sleep.eks_blueprints_addons]
}

resource "aws_iam_role" "ebs_addon" {
  name_prefix = "ebs-addon-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = module.eks.oidc_provider_arn
        }
        Condition = {
          StringEquals = {
            "${replace(module.eks.oidc_provider_arn, "/^(.*provider/)/", "")}:aud" = "sts.amazonaws.com"
            "${replace(module.eks.oidc_provider_arn, "/^(.*provider/)/", "")}:sub" = "system:serviceaccount:kube-system:ebs-csi-controller-sa"
          }
        }
      },
    ]
  })
}

resource "aws_iam_policy_attachment" "ebs_addon" {
  name       = "ebs-addon-attachment"
  roles      = [aws_iam_role.ebs_addon.id]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
}

resource "time_sleep" "eks_blueprints_addons" {
  create_duration  = "30s"
  destroy_duration = "30s" # buffer to delete ingress alb
  depends_on       = [module.eks_blueprints_addons]
}

resource "helm_release" "trust_manager" {
  name            = "trust-manager"
  namespace       = "cert-manager"
  chart           = "trust-manager"
  repository      = "https://charts.jetstack.io"
  atomic          = true
  cleanup_on_fail = true

  depends_on = [time_sleep.eks_blueprints_addons]
}

resource "time_sleep" "trust_manager" {
  create_duration = "30s"
  depends_on      = [helm_release.trust_manager]
}

resource "aws_kms_key" "vault_kms" {
  description             = "EKS ${var.environment} cluster Vault KMS key"
  deletion_window_in_days = 7
}

resource "aws_dynamodb_table" "vault_dynamo" {
  name         = "${local.cluster_name}-vault-table"
  hash_key     = "Path"
  range_key    = "Key"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "Path"
    type = "S"
  }

  attribute {
    name = "Key"
    type = "S"
  }
}

locals {
  vault_kms    = try(aws_kms_key.vault_kms.id, "")
  vault_dynamo = try(aws_dynamodb_table.vault_dynamo.id, "")
}

module "vault" {
  source = "aws-ia/eks-blueprints-addon/aws"

  name                 = "vault"
  namespace            = "vault"
  create_namespace     = true
  chart                = "${path.module}/helm/vault"
  atomic               = true
  cleanup_on_fail      = true
  dependency_update    = true
  set_irsa_names       = ["vault.server.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"]
  create_role          = true
  role_name            = "vault-sa"
  role_name_use_prefix = true
  role_description     = "IRSA for vault-sa"
  role_policies        = { PowerUserAccess = "arn:aws:iam::aws:policy/PowerUserAccess" }
  create_policy        = false

  oidc_providers = {
    this = {
      provider_arn    = module.eks.oidc_provider_arn
      service_account = "vault-sa"
    }
  }

  values = [
    <<-EOT
      vault:
        server:
          ingress:
            enabled: true
            ingressClassName: "nginx"
            hosts:
              - host: vault-${var.environment}.${var.route53_domain_name}
          ha:
            enabled: true
            replicas: 2
            config: |
              ui = true

              listener "tcp" {
                tls_disable = 1
                address = "[::]:8200"
                cluster_address = "[::]:8201"
              }

              storage "dynamodb" {
                ha_enabled = "true"
                region     = "${data.aws_region.current.id}"
                table      = "${local.vault_dynamo}"
              }

              service_registration "kubernetes" {}

              seal "awskms" {
                region     = "ap-southeast-2"
                kms_key_id = "${local.vault_kms}"
              }
    EOT
  ]

  depends_on = [time_sleep.trust_manager]
}

resource "time_sleep" "vault" {
  create_duration = "30s"
  depends_on      = [module.vault]
}

resource "helm_release" "prometheus" {
  name             = "prometheus"
  namespace        = "monitoring"
  chart            = "prometheus"
  repository       = "https://prometheus-community.github.io/helm-charts"
  atomic           = true
  cleanup_on_fail  = true
  create_namespace = true

  values = [
    <<-EOT
      server:
        ingress:
          enabled: true
          ingressClassName: nginx
          hosts:
            - prometheus-${var.environment}.${var.route53_domain_name}
        nodeSelector:
          topology.kubernetes.io/zone: ${data.aws_availability_zones.zones.names[0]}
    EOT
  ]

  depends_on = [time_sleep.eks_blueprints_addons]
}

resource "helm_release" "loki" {
  name             = "loki"
  namespace        = "monitoring"
  chart            = "loki-stack"
  repository       = "https://grafana.github.io/helm-charts"
  atomic           = true
  cleanup_on_fail  = true
  create_namespace = true

  depends_on = [time_sleep.eks_blueprints_addons]
}

resource "helm_release" "grafana" {
  name             = "grafana"
  namespace        = "monitoring"
  chart            = "grafana"
  repository       = "https://grafana.github.io/helm-charts"
  atomic           = true
  cleanup_on_fail  = true
  create_namespace = true

  values = [
    <<-EOT
      ingress:
        enabled: true
        ingressClassName: nginx
        hosts:
          - grafana-${var.environment}.${var.route53_domain_name}
      datasources:
        datasources.yaml:
          apiVersion: 1
          datasources:
            - name: Prometheus
              type: prometheus
              url: http://prometheus-server
            - name: Loki
              type: loki
              url: http://loki:3100
    EOT
  ]

  depends_on = [helm_release.loki, helm_release.prometheus]
}
